Standalone Cactus thorns: Pizza
-------------------------------

This is a working setup of how to compile Cactus
thorns out of Cactus scope. This works of course
only with thorns which do not make use of fancy
Cactus features such as the grid or parameters.
Pizza is such an example of a perfectly standalone
code.

Instructions: Replace the defect softlinks with
working ones pointing to your Cactus installation.
Hit ./make.sh and that's it.

-- SvenK, 2018-02-20

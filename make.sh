#!/bin/bash
#
# A quick and dirty build system for quasi-standalone cactus
# thorns in order to debug them.
#
# SvenK, 2018-02-20 for bnslt.

CFLAGS="-ggdb"
LFLAGS="-lgsl -lgslcblas -lm"

CC="g++ ${CFLAGS} -I../PizzaNumUtils_CactusHeaders/ -I../EOS_Barotropic/src -I../PizzaNumUtils/src"

set -ex
rm -rf build/ && mkdir -p build && cd build

for cppfile in ../{EOS_Barotropic,PizzaNumUtils}/src/*.cpp; do
	$CC -c $cppfile &
done

$CC -c ../main.cc

wait

g++ -o pizza_tester *.o ${LFLAGS}


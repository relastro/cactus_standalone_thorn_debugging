#include <stdio.h>
#include <string>
#include <iostream>

#include "eos_barotropic_file.h"

int main(int argc, char** argv) {
	if(argc<2) {
		printf("Usage: %s <pizza.h5 eos file>", argv[0]);
		return 1;
	}

	std::string eos_file = argv[1];

//	units u = Pizza::unitspizza_base_central::get().internal_units;
	EOS_Barotropic::eos_1p eos = EOS_Barotropic::load_eos_1p(eos_file);
	return 0;
}
